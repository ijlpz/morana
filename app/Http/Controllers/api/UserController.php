<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with('roles')->get();//paginate(10);
        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'last_name' => $request->get('last_name'),
            'dni' => $request->get('dni'),
            'gender' => $request->get('gender'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
            'cellphone' => $request->get('cellphone'),
        ]);

        if ($user){

            $user->syncRoles($request->get('role_id'));

            $photo = $request->file('photo');
            $custom_name = "";
            if (!is_null($photo)) {
                $dir = public_path()."/img/profiles/";
                $date = Carbon::now();
                $uuid = Str::uuid()->toString();
                $custom_name = "profile_img-".Str::uuid()->toString().'-'.$date->format('Y-m-d').'.'.$photo->getClientOriginalExtension();
                $photo->move($dir,$custom_name);
            }else{
                $custom_name = "default.jpg";
            }
            $user->update([
                'photo'=>$custom_name
            ]);
            return response()->json(['message'=>'Usuario registrado con exito'], 200);
        }
        return response()->json(['error'=>'Valores ingresados no validos'], 422);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}