<?php

namespace App\Http\Controllers\api;

use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $clients = Client::with('client_type:name,id')
            ->get();
        return  response()->json($clients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $client = Client::create([
            'ruc'=> $request->get('ruc'),
            'dni'=>$request->get('dni'),
            'full_name'=>$request->get('full_name'),
            'business_name'=>$request->get('business_name'),
            'address'=>$request->get('address'),
            'email'=> $request->get('email'),
            'cellphone'=> $request->get('cellphone'),
            'telephone'=> $request->get('telephone'),
            'client_type_id'=> $request->get('client_type_id'),
        ]);

        if($client){
            return response()->json(['message'=>'Cliente registrado'],200);
        }

        return response()->json(['error'=>'Los datos ingresados no son validos'],422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
