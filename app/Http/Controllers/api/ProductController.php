<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $products = Product::with('product_category')->get();
        return  response()->json($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $product = Product::create([
            'name'=>$request->get('name'),
            'product_category_id'=>$request->get('product_category_id'),
            'code'=>$request->get('code'),
            'color'=>$request->get('color'),
            'height'=>$request->get('height'),
            'width'=>$request->get('width'),
            'depth'=>$request->get('depth'),
            'diameter'=>$request->get(''),
            'thickness'=>$request->get('diameter'),
            'description'=>$request->get('description'),

        ]);

        if ($product){
            $image = $request->file('image');
            if (!is_null($image)){
                $dir =  public_path()."/img/products/";
                $date = Carbon::now();
                $uuid = Str::uuid()->toString();
                $custom_name = "product-img-".Str::uuid()->toString().'-'.$date->format('Y-m-d').'.'.$image->getClientOriginalExtension();
                $image->move($dir,$custom_name);
                $product->update([
                    'image' => $custom_name
                ]);
            }
            return response()->json(['message'=>'Producto registrado']);

        }
        return response()->json(['error'=>'Los valores ingresados no son validos'],422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCorrelativeCode(Request  $request){
        $code = $request->get('code');
        $count = 0;
        if (isset($code)){
            //$count = Product::where('code','like',"{$code}%")->count();
            $product = Product::where(DB::raw('substr(code, 1, 7)'),'=', (string)($code))->get()->last();
            if ($product){
                $code = $product->code;
                $size = strlen($code);
                $count = (int) substr($code,$size-3,$size);
                $count++;
            }
        }
        return response()->json(['count'=>$count]);

    }

    public function searchByNameOrCode(Request $request){
        $search = $request->get('search');
        if (!empty($search)){
            $products = Product::with('product_category')
                ->where('name','like',"%{$search}%")
                ->orWhere('code','like',"%{$search}%")
                ->get();
            return  response()->json(['products'=>$products]);
        }

        return  response()->json(['products'=>[]]);

    }
}
