<?php

namespace App\Http\Controllers;

use App\OrderContact;
use Illuminate\Http\Request;

class OrderContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderContact  $orderContact
     * @return \Illuminate\Http\Response
     */
    public function show(OrderContact $orderContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderContact  $orderContact
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderContact $orderContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderContact  $orderContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderContact $orderContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderContact  $orderContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderContact $orderContact)
    {
        //
    }
}
