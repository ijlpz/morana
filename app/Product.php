<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        'name',
        'description',
        'code',
        'color',
        'height',
        'width',
        'depth',
        'diameter',
        'thickness',
        'image',
        'product_category_id',
    ];

    public function product_category(){
        return $this->belongsTo(ProductCategory::class);
    }
}
