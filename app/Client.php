<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable=[
        'dni',
        'ruc',
        'full_name',
        'business_name',
        'address',
        'email',
        'cellphone',
        'telephone',
        'client_type_id'
    ];

    public function client_type(){
        return $this->belongsTo(ClientType::class);
    }
}
