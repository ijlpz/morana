<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('panel')->name('api.')->group(function (){
	# roles
    Route::apiResource('roles','api\RoleController')->only('index');

    # users
	Route::apiResource('users','api\UserController')->only('index','store');

	# clients
	Route::apiResource('clients','api\ClientController')->only('index','store');

	#client types
	Route::apiResource('client-types','api\ClientTypeController')->only('index');

	# categories for products
    Route::apiResource('product-categories','api\ProductCategoryController')->only('index', 'store');

    # products
    Route::apiResource('products','api\ProductController')->only('index','store');
    Route::get('products/correlative-code','api\ProductController@getCorrelativeCode');
    Route::get('products/search','api\ProductController@searchByNameOrCode');

});

