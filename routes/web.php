<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

//Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/', 'Auth\LoginController@login');
Route::post('salir', 'Auth\LoginController@logout')->name('logout');

// Registration Routes... (No available)
/*Route::get('registrate', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');*/

// Password Reset Routes...
/*Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');*/


/*****************
 * SPA routes
 ****************/

Route::middleware('auth:web')->group(function (){

//panel index
    Route::get('/panel', 'PanelController@index')->name('panel');

//orders
    Route::get('/panel/agregar-pedidos','PanelController@index')->name('orders.create');
    Route::get('/panel/listar-pedidos','PanelController@index')->name('orders.index');

//clients
    Route::get('/panel/agregar-clientes','PanelController@index')->name('clients.create');
    Route::get('/panel/listar-clientes','PanelController@index')->name('clients.index');

//users
    Route::get('/panel/agregar-usuarios','PanelController@index')->name('users.create');
    Route::get('/panel/listar-usuarios','PanelController@index')->name('users.index');

//products
    Route::get('/panel/agregar-productos','PanelController@index')->name('products.create');
    Route::get('/panel/listar-productos','PanelController@index')->name('products.index');

//categories
    Route::get('/panel/agregar-categorias','PanelController@index')->name('categories.create');
    Route::get('/panel/listar-categorias','PanelController@index')->name('categories.index');

//reports
    Route::get('/panel/agregar-reportes','PanelController@index')->name('reports.create');
    Route::get('/panel/listar-reportes','PanelController@index')->name('reports.index');

});
