$(document).ready(function() {

		    /*$('.tableResponsive').DataTable({
		    	"language": {
                	"decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Último",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }

            	}
		    });*/

			$('.mainNav').mouseenter(
				function(){
					if ($('.mainNav').hasClass('expanded')) {
					}
					else
					{
						$('.mainNav').addClass('expanded');
						$('section.content').addClass('expanded');
						$('.footerMorana').addClass('expanded');
					}
				}
			);
			$('.mainNav').mouseleave(
				function(){
					if (($('.mainNav').hasClass('expanded')) && ($('.menuButton').hasClass('menuOpen'))) {
					}
					else
					{
						$('.mainNav').removeClass('expanded');
						$('section.content').removeClass('expanded');
						$('.footerMorana').removeClass('expanded');
						$('a.dropdown-primary').removeClass('active');
						$('.dropdown-secondary').slideUp();
					}
				}

			);


			$('.menuButton').click(
				function(){
					if ($('.mainNav').hasClass('expanded')) {
						$('.menuButton').removeClass('menuOpen');
						$('.mainNav').removeClass('expanded');
						$('section.content').removeClass('expanded');
						$('.footerMorana').removeClass('expanded');
						$('a.dropdown-primary').removeClass('active');
						$('.dropdown-secondary').slideUp();

					}
					else{
						$('.mainNav').addClass('expanded');
						$('section.content').addClass('expanded');
						$('.footerMorana').addClass('expanded');
						$('.menuButton').addClass('menuOpen');
					}
					if ($('.infoUser').hasClass('active')) {
						$('.profileNav').slideUp();
						$('.infoUser').removeClass('active');
					}

				}
			);



			$('.dropdown-primary').click(
				function(){
					if ($(this).hasClass('active')) {
						$(this).siblings('ul').slideUp();
						$('.dropdown-primary').removeClass('active');
					}
					else
					{
						$(this).addClass('active');
						$(this).siblings('ul').slideDown();
						$(this).parent('.nav-item').siblings().children('.dropdown-primary').removeClass('active');
						$(this).parent('.nav-item').siblings().children('.dropdown-secondary').slideUp();
					}



				}
			);

			$('.dropdown-secondary li a').click(
				function(){
					if (!($(this).hasClass('active'))) {
						$('.dropdown-secondary li a').removeClass('active');
						$(this).addClass('active');
					}
				}
			);


			$('.infoUser').click(
				function(){
					if ($(this).hasClass('active')) {
						$('.profileNav').slideUp();
						$('.infoUser').removeClass('active');
					}
					else
					{
						$('.infoUser').addClass('active');
						$('.profileNav').slideDown();
					}
				}
			);




			/*ALERTAS*/
			var heightAlerts = $('.alerts').height();
			var heightBoxAlert = $('.boxAlert').height()+40;
			var margTopAlert = (heightAlerts-heightBoxAlert)/2;
			$('.boxAlert').css('margin-top',margTopAlert);

			var heightWindows = $(window).height();
			var minHeightMorana = heightWindows - 83;
			$('section.content').css('min-height',minHeightMorana);


			/*/!*COLOUR PICKER*!/
			$('#color-picker').spectrum({
			  type: "color",
			  showPalette: "false",
			  showAlpha: "false",
			  allowEmpty: "false"
			});*/

			/*PRUEBAS*/
			//$('.xyz').click(
			//	function(){
			//		 $('.abc').click();
			//	}
			//);

			/*RESET IMAGE*/
			$('.resetImage').on('click', function(e) {
			      var $el = $('.addImage');
			      $el.wrap('<form>').closest('form').get(0).reset();
			      $el.unwrap();
			});

			/*Select Cliente tipo*/

			$('.customersType select option').click(
				function(){
					console.log('sss');
					if ($('.customersType select option:selected').text()=="Persona Natural") {
						$('.optionRUC').hide();
						$('.optionDNI').show();
					}
					if ($('.customersType select option:selected').text()=="Persona Jurídica") {
						$('.optionDNI').hide();
						$('.optionRUC').show();

					}
				}
			);

			$('span.rememberMeLabel').click(
				function(){
					$('input.remember').click();
			});
			$('span.rememberCheck').click(
				function(){
					$('input.remember').click();
			});

			/*ORDERS*/
			var hsc1 = $('.sc1').height();
			$('.contentOrdersFull').css('height', hsc1);

			$('.buttonNext.step1Button').click(
				function(){
					var hsc2 = $('.sc2').height();
					$('.contentOrdersFull').css('height', hsc2);
					$('.contentOrdersFull.step1Active').addClass('step2Active');
					$('.steps .step.step1.active').addClass('full');
					$('.steps .step.step2').addClass('active');
			});
			$('.buttonNext.step2Button').click(
				function(){
					var hsc3 = $('.sc3').height();
					$('.contentOrdersFull').css('height', hsc3);
					$('.contentOrdersFull.step1Active').addClass('step3Active');
					$('.steps .step.step2.active').addClass('full');
					$('.steps .step.step3').addClass('active');
			});
			$('.buttonNext.step3Button').click(
				function(){
					var hsc4 = $('.sc4').height();
					$('.contentOrdersFull').css('height', hsc4);
					$('.contentOrdersFull.step1Active').addClass('step4Active');
					$('.steps .step.step3.active').addClass('full');
					$('.steps .step.step4').addClass('active');
			});

			var removeClassOrders = function (){
				$('.contentOrdersFull').removeClass('step2Active');
				$('.contentOrdersFull').removeClass('step3Active');
				$('.contentOrdersFull').removeClass('step4Active');
			}
			$('.steps .step.step1').click(
				function(){
					if ($(this).hasClass('active')) {
						var hsc1 = $('.sc1').height();
						$('.contentOrdersFull').css('height', hsc1);
						removeClassOrders();
					}
			});
			$('.steps .step.step2').click(
				function(){
					if ($(this).hasClass('active')) {
						var hsc2 = $('.sc2').height();
						$('.contentOrdersFull').css('height', hsc2);
						removeClassOrders();
						$('.contentOrdersFull').addClass('step2Active');
					}
			});
			$('.steps .step.step3').click(
				function(){
					if ($(this).hasClass('active')) {
						var hsc3 = $('.sc3').height();
						$('.contentOrdersFull').css('height', hsc3);
						removeClassOrders();
						$('.contentOrdersFull').addClass('step3Active');
					}
			});
			$('.steps .step.step4').click(
				function(){
					if ($(this).hasClass('active')) {
						var hsc4 = $('.sc4').height();
						$('.contentOrdersFull').css('height', hsc4);
						removeClassOrders();
						$('.contentOrdersFull').addClass('step4Active');
					}
			});


} );


