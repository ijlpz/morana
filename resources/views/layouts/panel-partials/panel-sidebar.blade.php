<div class="mainNav">
    <nav>
        <ul>
            <li class="nav-item orders">
                <a class="dropdown-primary" href="javascript:void(0)">
							<span class="icon">
								<i class="icon-orders c-fuchsia"></i>
							</span>
                    <span class="title c1">Pedidos</span>
                    <span class="arrow c2">
								<i class="icon-arrow-right"></i>
							</span>
                </a>
                <ul class="dropdown-secondary">
                    <li><router-link :to="{name:'orders.index'}" class="c2">Listar Pedidos</router-link></li>
                    <li><router-link :to="{name: 'orders.create'}" class="c2">Agregar Pedidos</router-link></li>
                </ul>
            </li>
            <li class="nav-item customers">
                <a class="dropdown-primary" href="javascript:void(0)">
							<span class="icon">
								<i class="icon-customers c-blue"></i>
							</span>
                    <span class="title c1">Clientes</span>
                    <span class="arrow c2">
								<i class="icon-arrow-right"></i>
							</span>
                </a>
                <ul class="dropdown-secondary">
                    <li><router-link :to="{name: 'clients.index'}" class="c2">Listar Clientes</router-link></li>
                    <li><router-link :to="{name: 'clients.create'}" class="c2">Agregar Clientes</router-link></li>
                </ul>
            </li>
            <li class="nav-item users">
                <a class="dropdown-primary" href="javascript:void(0)">
							<span class="icon">
								<i class="icon-users c-gray"></i>
							</span>
                    <span class="title c1">Usuarios</span>
                    <span class="arrow c2">
								<i class="icon-arrow-right"></i>
							</span>
                </a>
                <ul class="dropdown-secondary">
                    <li><router-link :to="{name: 'users.index'}" class="c2">Listar Usuarios</router-link></li>
                    <li><router-link  :to="{name: 'users.create'}" class="c2">Agregar Usuarios</router-link></li>
                </ul>
            </li>
            <li class="nav-item products">
                <a class="dropdown-primary" href="javascript:void(0)">
							<span class="icon">
								<i class="icon-products c-orange"></i>
							</span>
                    <span class="title c1">Productos</span>
                    <span class="arrow c2">
								<i class="icon-arrow-right"></i>
							</span>
                </a>
                <ul class="dropdown-secondary">
                    <li><router-link :to="{name: 'products.index'}" class="c2">Listar Productos</router-link></li>
                    <li><router-link :to="{name: 'products.create'}" class="c2">Agregar Productos</router-link></li>
                </ul>
            </li>
            <li class="nav-item categories">
                <a class="dropdown-primary" href="javascript:void(0)">
							<span class="icon">
								<i class="icon-categories c-fuchsia"></i>
							</span>
                    <span class="title c1">Categorías</span>
                    <span class="arrow c2">
								<i class="icon-arrow-right"></i>
							</span>
                </a>
                <ul class="dropdown-secondary">
                    <li><router-link :to="{name: 'categories.index'}" class="c2">Listar Categorías</router-link></li>
                    <li><router-link :to="{name: 'categories.create'}" class="c2">Agregar Categorías</router-link></li>
                </ul>
            </li>
            <li class="nav-item reports">
                <a class="dropdown-primary" href="javascript:void(0)">
							<span class="icon">
								<i class="icon-reports c-green"></i>
							</span>
                    <span class="title c1">Reportes</span>
                    <span class="arrow c2">
								<i class="icon-arrow-right"></i>
							</span>
                </a>
                <ul class="dropdown-secondary">
                    <li><router-link :to="{name: 'reports.index'}" class="c2">Listar Reportes</router-link></li>
                    <li><router-link :to="{name: 'reports.create'}" class="c2">Agregar Reportes</router-link></li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
