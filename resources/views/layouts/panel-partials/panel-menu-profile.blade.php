<div class="profileNav">
    <ul>
        <li>
            <a class="nav-profile c2" href="#">
                <span class="icon"><i class="icon-bookmark"></i></span>
                <span>Mi Perfil</span>
            </a>
        </li>
        <li>
            <a class="nav-setting c2" href="#">
                <span class="icon"><i class="icon-setting"></i></span>
                <span>Configuración</span>
            </a>
        </li>
        <li>
            <a class="nav-activity c2" href="#">
                <span class="icon"><i class="icon-eye"></i></span>
                <span>Actividad</span>
            </a>
        </li>
        <li>
            <a class="nav-logout c2" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <span class="icon"><i class="icon-logout"></i></span>
                <span>Cerrar Sesión</span>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </li>
    </ul>
</div>
