@php echo '<'.'?xml version="1.0" encoding="utf-8"?'.'>'; @endphp
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Morana Administrator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    {{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>

    <!---->
    <script src="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2@2.0.0/dist/spectrum.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2@2.0.0/dist/spectrum.min.css">--}}
    <!---->
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <script src="{{mix('js/app.js')}}" defer></script>

</head>
<body>
<div id="app">
    <!--HEADER -->
    @include('layouts.panel-partials.panel-header')
    <!--HEADER -->

    @include('layouts.notifications.alert')

    <!--MENU LATERAL -->
    @include('layouts.panel-partials.panel-sidebar')
    <!--MENU LATERAL -->

    <!--MENU PERFIL -->
    @include('layouts.panel-partials.panel-menu-profile')
    <!--MENU PERFIL -->



    <!-- SECCIÓN VARIABLE -->

    <!--SECCION AGREGAR PEDIDOS -->
    @yield('content')
    <!--SECCION AGREGAR PEDIDOS -->



    <!-- SECCIÓN VARIABLE -->
</div>


<!--FOOTER-->
@include('layouts.panel-partials.panel-footer')
<!--FOOTER-->


</body>
</html>