<!--ALERTAS -->
@if(session()->has('warning'))
<div class="alerts alertShow warning">
    <div class="boxAlert">
        <div class="closeAlert"><i class=""></i></div>
        <div class="contentAlert">
            <h3 class="titleAlert">¡este es el título de la alerta!</h3>
            <p>Este es el contenido de la alerta, aquí se pondrá todo lo relacionado a información de confirmaciones, advertencias, errores, etc.</p>
            <div class="buttonsAlert">
                <button class="btnAlertPrimary">Aceptar</button>
                <button class="btnAlertSecondary">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@endif
<!--ALERTAS -->


@if(session()->has('suggestion'))
<div class="alerts alertShow suggestion">
    <div class="boxAlert">
        <div class="closeAlert"><i class=""></i></div>
        <div class="contentAlert">
            <h3 class="titleAlert">¡este es el título de la alerta!</h3>
            <p>Este es el contenido de la alerta, aquí se pondrá todo lo relacionado a información de confirmaciones, advertencias, errores, etc.</p>
            <div class="buttonsAlert">
                <button class="btnAlertPrimary">Aceptar</button>
                <button class="btnAlertSecondary">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@endif



@if(session()->has('information'))
<div class="alerts alertShow information">
    <div class="boxAlert">
        <div class="closeAlert"><i class=""></i></div>
        <div class="contentAlert">
            <h3 class="titleAlert">¡este es el título de la alerta!</h3>
            <p>Este es el contenido de la alerta, aquí se pondrá todo lo relacionado a información de confirmaciones, advertencias, errores, etc.</p>
            <div class="buttonsAlert">
                <button class="btnAlertPrimary">Aceptar</button>
                <button class="btnAlertSecondary">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@endif

