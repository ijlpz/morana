@extends('layouts.auth')

@section('content')
    <login-component></login-component>
   {{-- <form method="POST" action="{{ route('login') }}">
        @csrf
        <p>
            <span class="label">DNI:</span>
            <span class="inputs">
                <input type="text" class="@error('dni') is-invalid @enderror" value="{{ old('dni') }}" name="dni" autocomplete="off">
                @error('dni')
                <span class="alertform">{{ $message }}</span>
                @enderror
            </span>

        </p>
        <p>
            <span class="label">Contraseña:</span>
            <span class="inputs">
                <input type="password"  name="password">
                @error('password')
                <span class="alertform">{{ $message }}</span>
                @enderror
            </span>
        </p>
        <p>
            <span class="label rememberMe"></span>
            <span class="inputs">
                 <input class="remember" type="checkbox" value="">
                 <span class="rememberCheck"></span>
                 <span class="rememberMeLabel">Recordar contraseña</span>
            </span>
        </p>
        <p>
            <button type="submit">INGRESAR</button>
        </p>
    </form>
    <p class="forgotPassword">
        <a href="#">¿Olvidaste tu contraseña?</a>
    </p>--}}
{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
