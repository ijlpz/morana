/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Vee-Validate
 */

import { ValidationProvider , ValidationObserver, } from 'vee-validate/dist/vee-validate.full.esm';
import { configure } from 'vee-validate/dist/vee-validate.full.esm';
// import { validate } from 'vee-validate/dist/vee-validate.full.esm';
import { setInteractionMode } from 'vee-validate/dist/vee-validate.full.esm';
setInteractionMode('eager');

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
configure({
    classes: {
        valid: 'is-valid',
        invalid: 'is-invalid',
    }
})

import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

import 'datatables.net';
import 'datatables.net-bs4';
import 'datatables.net-responsive';
import 'datatables.net-responsive-bs4';

import 'spectrum-colorpicker2/dist/spectrum'

/**
 * Vue Router
 */

import router from './assets/router';

/**
 * Vuex
 */

import store from './assets/store'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('login-component', require('./components/auth/Login-Component').default);
Vue.component('alert-component', require('./components/notifications/AlertsComponent').default);

Vue.component('users-table-component', require('./components/panel/users/Users-Table-Component').default);
Vue.component('clients-table-component', require('./components/panel/clients/Clients-Table-Component').default);
Vue.component('product-categories-table-component', require('./components/panel/product-categories/ProductCategories-Table-Component').default);
Vue.component('products-table-component', require('./components/panel/products/Products-Table-Component').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
});
