window.Vue = require('vue');
import VueRouter from "vue-router";

//panel
import  PanelIndex from '../views/panel/Panel-Index-View'

//orders
import OrdersCreate from '../views/panel/orders/Orders-Create-View';
import OrdersList from '../views/panel/orders/Orders-List-View';

//clients
import ClientsCreate from '../views/panel/clients/Clients-Create-View';
import ClientsList from '../views/panel/clients/Clients-List-View';

//users
import UsersCreate from '../views/panel/users/Users-Create-View';
import UsersList from '../views/panel/users/Users-List-View';

//products
import ProductsCreate from '../views/panel/products/Products-Create-View';
import ProductsList from '../views/panel/products/Products-List-View';

//categories
import CategoriesCreate from '../views/panel/categories/Categories-Create-View';
import CategoriesList from '../views/panel/categories/Categories-List-View';

//reports
import ReportsCreate from '../views/panel/reports/Reports-Create-View';
import ReportsList from '../views/panel/reports/Reports-List-View';

//404
import Error404 from '../views/errors/Error404'

Vue.use(VueRouter);
export default new VueRouter({
    mode: 'history',
    routes:[
        {path: '/panel', component: PanelIndex, name:'panel.index'},

        //orders
        {path: '/panel/agregar-pedidos', component: OrdersCreate, name:'orders.create'},
        {path: '/panel/listar-pedidos', component: OrdersList, name:'orders.index'},

        //clients
        {path: '/panel/agregar-clientes', component: ClientsCreate, name:'clients.create'},
        {path: '/panel/listar-clientes', component: ClientsList, name:'clients.index'},

        //users
        {path: '/panel/agregar-usuarios', component: UsersCreate, name:'users.create'},
        {path: '/panel/listar-usuarios', component: UsersList, name:'users.index'},

        //products
        {path: '/panel/agregar-productos', component: ProductsCreate, name:'products.create'},
        {path: '/panel/listar-productos', component: ProductsList, name:'products.index'},

        //categories
        {path: '/panel/agregar-categorias', component: CategoriesCreate, name:'categories.create'},
        {path: '/panel/listar-categorias', component: CategoriesList, name:'categories.index'},

        //reports
        {path: '/panel/agregar-reportes', component: ReportsCreate, name:'reports.create'},
        {path: '/panel/listar-reportes', component: ReportsList, name:'reports.index'},


       /* { path : '/', component: PostList, name:'list' },
        { path : '/detail/:id', component: PostDetail, name: 'detail' },
        { path : '/post-category/:category_id', component: PostCategory, name: 'post-category' },

        { path : '/contact', component: ContactComponent, name: 'contact' },*/
        {path: '*', component: Error404 }

    ],
    linkExactActiveClass:'active'
});
