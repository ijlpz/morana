<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();

            //columns
            $table->char('dni',8)->nullable();
            $table->char('ruc',11)->nullable();
            $table->string('full_name')->nullable();
            $table->string('business_name')->nullable();
            $table->string('address');
            $table->string('email');
            $table->string('cellphone')->nullable();
            $table->string('telephone')->nullable();

            //fk
            $table->foreignId('client_type_id')->references('id')->on('client_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
