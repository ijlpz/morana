<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            //columns
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('code');
            $table->string('color')->nullable();

            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('diameter')->nullable();
            $table->integer('thickness')->nullable();

            $table->string('image')->nullable();

            //fk
            $table->foreignId('product_category_id')->references('id')->on('product_categories');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }

    /**
     * @code:
     * 2 primeras letras de la primera palabra  del producto
     * 2 primeras de la segunda palabra del producto
     * 3 primeras letras de la categoría
     * el correlativo 0001
     */
}
