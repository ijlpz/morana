<?php

use App\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductCategory::create([
            'name'=>'Madera',
            'code'=>'MAD',
            'description'=>'Muebles hechos en madera.'
        ]);

        ProductCategory::create([
            'name'=>'Melamine',
            'code'=>'MEL',
            'description'=>'Muebles hechos en melamine.'
        ]);

        ProductCategory::create([
            'name'=>'Metal',
            'code'=>'MET',
            'description'=>'Muebles hechos en metal.'
        ]);

        ProductCategory::create([
            'name'=>'Otro',
            'code'=>'OTR',
            'description'=>'Muebles hechos en otro tipo de material.'
        ]);
    }
}
