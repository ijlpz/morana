<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt('123123123'),
            'last_name'=>'Apellido Apellido',
            'dni'=>'88889999',
            'gender'=>'male',
            'photo'=>'default.jpg',
            'address'=>'Direccion',
            'phone'=>'073-333344',
            'cellphone'=>'998877665',

        ]);
    }
}
