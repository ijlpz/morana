<?php

use App\ClientType;
use Illuminate\Database\Seeder;

class ClientTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClientType::create([
            'name'=>'Persona Natural'
        ]);

        ClientType::create([
            'name'=>'Persona Juridica'
        ]);
    }
}
